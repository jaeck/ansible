# Ansible使用手册

Ansible是新出现的自动化运维工具，基于Python开发，集合了众多运维工具（puppet、cfengine、chef、func、fabric）的优点，实现了批量系统配置、批量程序部署、批量运行命令等功能。



此文档提供了基础环境的安装(系统初始化/lua环境/java环境)及一些所需应用的安装(redis, zookeeper, kafka, tomcat)。



**如果需要采用ansbile部署, 首先需要一台机器安装ansible软件, 并且与需要安装机器的ssh打通.(ssh免密钥)**



### ansible安装(以Centos7.4 1804 系统为例)：

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
# 默认安装目录 /etc/ansible

[root@node1 ~]# rm -rf /etc/ansible
[root@node1 ~]# cd /etc/
[root@node1 etc]# git clone ssh://git@69.172.89.221:18822/huangxiang/ansible.git
[root@node1 etc]# cd ansible
[root@node1 etc]# ls
ansible.cfg  doc  hosts  roles  site.yaml
```



### 简单介绍下hosts文件和site.yaml文件，以便于解读doc下的安装文档.




**hosts**

Ansible 可同时操作属于一个组的多台主机,组和主机之间的关系通过 inventory 文件配置. 默认的文件路径为 /etc/ansible/hosts



**以下代码解析：**
*   定义一个tomcat的组，里面有2台主机，分别为 192.168.1.189/190

```shell

[root@node1 ansible]# more hosts 
[tomcat]
192.168.1.189
192.168.1.190
```

hosts文件配置可以参考：http://www.ansible.com.cn/docs/intro_inventory.html




**site.yaml**

**以下代码解析：**
*   hosts: 表示需要执行的机器为tomcat组里面的机器。(上面hosts文件配置的tomcat组里面的机器)
*   remote_user: root 这个可以默认，不用去修改。
*   roles：表示hosts: tomcat组里面的机器，都需要去干的事情。这里的roles: tomcat表示, tomcat角色应该做的事情。
  
```shell
[root@node1 ansible]# more site.yaml 
- hosts: tomcat 
  remote_user: root
  roles:
  - tomcat
```



**/etc/ansible/roles 下面会有很多角色，代表某个角色应该做的事情。具体请看 tasks/main.yaml**

```
[root@node1 ansible]# ls -l roles/
total 72
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 add-codis-proxy
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 codis-cluster
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 codis-dashboard-proxy-fe
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 codis-fe
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 codis-proxy
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 codis-sentinel
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 common
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 go
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 jdk
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 kafka
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 keepalived
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 lua_env
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 mongodb
drwxr-xr-x. 4 root root 4096 Jun 13 15:13 redis
drwxr-xr-x. 4 root root 4096 Jun 13 15:49 redis_slave_6379
drwxr-xr-x. 4 root root 4096 Jun 13 15:51 redis_slave_6380
drwxr-xr-x. 4 root root 4096 Jun 13 18:44 tomcat
drwxr-xr-x. 5 root root 4096 Jun 13 15:13 zookeeper
```



**举例： jdk roles**

```shell
[root@node1 ansible]# tree roles/jdk/
roles/jdk/
├── files
│   └── jdk-8u151-linux-x64.tar.gz
├── tasks
│   └── main.yaml
└── templates
    └── profile

3 directories, 3 files

[root@node1 ansible]# more roles/jdk/tasks/main.yaml 
- name: mkdir /data/app
  file: path=/data/app state=directory

- name: unzip jdk package
  unarchive: src=jdk-8u151-linux-x64.tar.gz dest=/data/app/

- name: mv /data/app/jdk1.8.0_151 /data/app/jdk
  shell: rm -rf /data/app/jdk; cd /data/app && mv jdk1.8.0_151 jdk

- name: config env java
  template: src=profile dest=/etc/profile
```



**以上jdk roles下的tasks/main.yaml 其实就是安装jdk的步骤，采用yaml格式编写。**











