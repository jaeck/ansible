# 部署redis环境（6379,6380）

## 环境准备

|         hostname          |      ip       | 系统           |
| :-----------------------: | :-----------: | :------------- |
| node1(ansible, redis6379) | 192.168.1.188 | Centos7.4 1804 |
|     node2(redis6380)      | 192.168.1.189 | Centos7.4 1804 |

**注意： 采用ansible一键部署redis环境**

* 系统环境：Centos7 x64 1804
* 各应用安装目录 /data/app
* 纯净系统
* redis6379 和 redis6380主要服务于lua环境和java环境**（此redis为redis从）**



## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# rm -rf /etc/ansible
[root@node1 ~]# cd /etc/
[root@node1 etc]# git clone ssh://git@69.172.89.221:18822/huangxiang/ansible.git
[root@node1 etc]# ls /etc/ansible/
ansible.cfg  doc  hosts  roles  site.yaml
```



**配置ssh免秘钥认证, 在node1上操作.（如果已经ssh打通，则不需要配置此项.）**

```shell
# 运行命令, 直接回车三次即可. 如果原来已经有，可以不生成秘钥。
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到两个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.
```



**配置hosts(根据实际情况配置, 以下配置以当前环境配置为例)**

```shell
# 192.168.1.188/189/190 修改hostname
[root@node1 ~]# hostnamectl set-hostname node1
[root@node2 ~]# hostnamectl set-hostname node2
[root@node3 ~]# hostnamectl set-hostname node3

[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
```



**配置/etc/ansbile**

* **把192.168.1.189 替换成实际ip地址(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
# 需要安装基础环境和lua环境的机器，都添加到[lua_env]下，一行写一个ip地址。
# ssh端口默认22, 如果端口修改成18822，请在ip后面加上 ansible_ssh_port=18822 (和ip之前须有空格)
[root@node1 ~]# vim /etc/ansible/hosts
[redis_slave_6379]
192.168.1.188 ansible_ssh_port=18822

[redis_slave_6380]
192.168.1.189

```



**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ansbile]# pwd
/etc/ansible

# hosts 代表是 /etc/ansible/hosts 文件下的，[redis_slave_6379] 下所有机器执行操作。
# remote_user 远程执行用户为root
# roles 代表会引用redis_slave_6379角色，会执行角色下所有操作
[root@node1 ansible]# more site.yaml 
- hosts: redis_slave_6379
  remote_user: root
  roles:
  - redis_slave_6379

- hosts: redis_slave_6380
  remote_user: root
  roles:
  - redis_slave_6380
  
# 首先测试下语法是否有问题
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check
playbook: site.yaml

# 然后开始运行
[root@node1 ansible]# ansible-playbook site.yaml

# 没有失败代表没问题，最后一步就是验证.
```

## 验证

```shell
[root@1_188 ansible]# ansible-playbook site.yaml

PLAY [redis_slave_6379] **************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************
ok: [192.168.1.188]

TASK [redis_slave_6379 : modify language] ********************************************************************************************************
changed: [192.168.1.188]

TASK [redis_slave_6379 : modify timezone] ********************************************************************************************************
 [WARNING]: Consider using file module with state=link rather than running ln

changed: [192.168.1.188]

TASK [redis_slave_6379 : install epel-release] ***************************************************************************************************
changed: [192.168.1.188]

TASK [redis_slave_6379 : install package] ********************************************************************************************************
ok: [192.168.1.188] => (item=[u'gcc', u'gcc-c++'])

TASK [redis_slave_6379 : mkdir /data/app] ********************************************************************************************************
ok: [192.168.1.188]

TASK [redis_slave_6379 : tar xf redis6379.tar.gz] ************************************************************************************************
changed: [192.168.1.188]

TASK [redis_slave_6379 : configuration redis-cli redis-server] ***********************************************************************************
changed: [192.168.1.188]

TASK [redis_slave_6379 : configuration redis6379.service] ****************************************************************************************
changed: [192.168.1.188]

TASK [redis_slave_6379 : systemctl daemon-reload] ************************************************************************************************
changed: [192.168.1.188]

PLAY [redis_slave_6380] **************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************
ok: [192.168.1.189]

TASK [redis_slave_6380 : modify language] ********************************************************************************************************
changed: [192.168.1.189]

TASK [redis_slave_6380 : modify timezone] ********************************************************************************************************
changed: [192.168.1.189]

TASK [redis_slave_6380 : install epel-release] ***************************************************************************************************
ok: [192.168.1.189]

TASK [redis_slave_6380 : install package] ********************************************************************************************************
ok: [192.168.1.189] => (item=[u'gcc', u'gcc-c++'])

TASK [redis_slave_6380 : mkdir /data/app] ********************************************************************************************************
ok: [192.168.1.189]

TASK [redis_slave_6380 : tar xf redis6380.tar.gz] ************************************************************************************************
changed: [192.168.1.189]

TASK [redis_slave_6380 : configuration redis-cli redis-server] ***********************************************************************************
changed: [192.168.1.189]

TASK [redis_slave_6380 : configuration redis6380.service] ****************************************************************************************
changed: [192.168.1.189]

TASK [redis_slave_6380 : systemctl daemon-reload] ************************************************************************************************
changed: [192.168.1.189]

PLAY RECAP ***************************************************************************************************************************************
192.168.1.188              : ok=10   changed=7    unreachable=0    failed=0   
192.168.1.189              : ok=10   changed=6    unreachable=0    failed=0
```



**最后一步配置，修改redis.conf中的监听的ip地址和slave of 配置(根据实际情况配置)**

```shell
[root@node1 ~]# grep  -E '^bind|slaveof' /data/app/redis6379/redis.conf
bind 127.0.0.1 192.168.1.189
# Master-Slave replication. Use slaveof to make a Redis instance a copy of
slaveof 192.168.1.189 6379

[root@node2 app]# grep  -E '^bind|slaveof' /data/app/redis6380/redis.conf
bind 127.0.0.1 192.168.1.189
# Master-Slave replication. Use slaveof to make a Redis instance a copy of
slaveof 192.168.1.189 6380
```



**启动redis(默认无密码，自行修改)**

```shell
[root@node1 ~]# systemctl start redis6379
[root@node2 ~]# systemctl start redis6380

# 设置开启自动启动
[root@node1 ~]# systemctl enable redis6379
[root@node2 ~]# systemctl enable redis6380

```