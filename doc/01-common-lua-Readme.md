# 部署基础环境和lua环境

## 环境准备

|    hostname    |      ip       | 系统           |
| :------------: | :-----------: | :------------- |
| node1(ansible) | 192.168.1.188 | Centos7.4 1804 |
|     node2      | 192.168.1.189 | Centos7.4 1804 |
|     node3      | 192.168.1.190 | Centos7.4 1804 |

**注意： 采用ansible一键部署基础环境和lua环境**

* **系统环境：Centos7 x64 1804**
* **各应用安装目录 /data/app**
* **纯净系统**
* **所有需要安装基础环境的机器，可以把ip地址写在/etc/ansible/hosts中(ssh打通)**


## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# rm -rf /etc/ansible
[root@node1 ~]# cd /etc/
[root@node1 etc]# git clone ssh://git@69.172.89.221:18822/huangxiang/ansible.git
[root@node1 etc]# ls /etc/ansible/
ansible.cfg  doc  hosts  roles  site.yaml

```


**配置ssh免秘钥认证, 在node1上操作.（如果已经ssh打通，则不需要配置此项.）**

```shell
# 运行命令, 直接回车三次即可. 如果原来已经有，可以不生成秘钥。
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到三个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189
[root@node1 ~]# ssh-copy-id root@192.168.1.190

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.

[root@node1 ~]# ssh root@192.168.1.190
Last login: Fri May  4 14:00:45 2018 from 192.168.1.190
[root@node2 ~]# exit
logout
Connection to 192.168.1.190 closed.
```



**配置hosts(根据实际情况配置, 以下配置以当前环境配置为例)**

```shell
# 192.168.1.188/189/190 修改hostname
[root@node1 ~]# hostnamectl set-hostname node1
[root@node2 ~]# hostnamectl set-hostname node2
[root@node3 ~]# hostnamectl set-hostname node3

[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
192.168.1.190 node3
```



**配置/etc/ansbile**

* **把192.168.1.189 替换成实际ip地址(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
# 需要安装基础环境和lua环境的机器，都添加到[lua_env]下，一行写一个ip地址。
# ssh端口默认22, 如果端口修改成18822，请在ip后面加上 ansible_ssh_port=18822 (和ip之前须有空格)
[root@node1 ~]# vim /etc/ansible/hosts
[lua_env]
192.168.1.188
192.168.1.189
192.168.1.190 
# 192.168.1.189 ansible_ssh_port=18822
```



**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ansbile]# pwd
/etc/ansible

# hosts 代表是 /etc/ansible/hosts 文件下的，[lua_env] 下所有机器执行操作。
# remote_user 远程执行用户为root
# roles 代表会引用3个角色，分别为common基础环境配置和lua环境配置和java环境(也可以只安装基础环境)
[root@node1 ansible]# more site.yaml 
- hosts: lua_env
  remote_user: root
  roles:
  - common
  - jdk
  - lua_env
  
# 首先测试下语法是否有问题
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check
playbook: site.yaml

# 然后开始运行
[root@node1 ansible]# ansible-playbook site.yaml

# 没有失败代表没问题，最后一步就是验证.
```

## 验证

```shell
[root@node1 ansible]# ansible-playbook site.yaml

PLAY [lua_env] ***************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *******************************************************************************************************************************************************************************************
ok: [192.168.1.190]
ok: [192.168.1.189]
ok: [192.168.1.188]

TASK [common : install epel-release] *****************************************************************************************************************************************************************************
ok: [192.168.1.189]
ok: [192.168.1.188]
ok: [192.168.1.190]

TASK [common : modify language] **********************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : modify timezone] **********************************************************************************************************************************************************************************
 [WARNING]: Consider using file module with state=link rather than running ln

changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : install  Development Tools group] *****************************************************************************************************************************************************************
 [WARNING]: Consider using yum module rather than running yum

changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [common : remve firewalld package] **************************************************************************************************************************************************************************
ok: [192.168.1.189] => (item=[u'firewalld', u'firewalld-filesystem', u'python-firewall'])
ok: [192.168.1.190] => (item=[u'firewalld', u'firewalld-filesystem', u'python-firewall'])
ok: [192.168.1.188] => (item=[u'firewalld', u'firewalld-filesystem', u'python-firewall'])

TASK [common : install package] **********************************************************************************************************************************************************************************
changed: [192.168.1.190] => (item=[u'gcc', u'gcc-c++', u'openssl-devel', u'lrzsz', u'wget', u'make', u'unzip', u'zip', u'xz', u'ntpdate', u'lsof', u'telnet', u'vim', u'tree', u'kernel', u'kernel-devel', u'iftop', u'iputils', u'net-tools', u'nload', u'tcpdump', u'nc', u'nmap'])
changed: [192.168.1.189] => (item=[u'gcc', u'gcc-c++', u'openssl-devel', u'lrzsz', u'wget', u'make', u'unzip', u'zip', u'xz', u'ntpdate', u'lsof', u'telnet', u'vim', u'tree', u'kernel', u'kernel-devel', u'iftop', u'iputils', u'net-tools', u'nload', u'tcpdump', u'nc', u'nmap'])
changed: [192.168.1.188] => (item=[u'gcc', u'gcc-c++', u'openssl-devel', u'lrzsz', u'wget', u'make', u'unzip', u'zip', u'xz', u'ntpdate', u'lsof', u'telnet', u'vim', u'tree', u'kernel', u'kernel-devel', u'iftop', u'iputils', u'net-tools', u'nload', u'tcpdump', u'nc', u'nmap'])

TASK [common : set UseDNS no for ssh] ****************************************************************************************************************************************************************************
 [WARNING]: Consider using template or lineinfile module rather than running sed

changed: [192.168.1.188]
changed: [192.168.1.190]
changed: [192.168.1.189]

TASK [common : set timeout time for ssh] *************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [common : restart sshd] *************************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : set /etc/hosts] ***********************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : set selinux off] **********************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : set limits.conf] **********************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
ok: [192.168.1.188]

TASK [common : set rc.local] *************************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
ok: [192.168.1.188]

TASK [common : ulimit -SHn 65535] ********************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [common : set sysctl.conf] **********************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
ok: [192.168.1.188]

TASK [common : sysctl -p] ****************************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [common : set crontab ntpdate] ******************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
ok: [192.168.1.188]

TASK [common : set time] *****************************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [common : stop postfix] *************************************************************************************************************************************************************************************
changed: [192.168.1.188]
changed: [192.168.1.189]
changed: [192.168.1.190]

TASK [jdk : mkdir /data/app] *************************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
ok: [192.168.1.188]

TASK [jdk : unzip jdk package] ***********************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [jdk : mv /data/app/jdk1.8.0_151 /data/app/jdk] *************************************************************************************************************************************************************
 [WARNING]: Consider using file module with state=absent rather than running rm

changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [jdk : config env java] *************************************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [lua_env : install epel-release] ****************************************************************************************************************************************************************************
ok: [192.168.1.189]
ok: [192.168.1.190]
ok: [192.168.1.188]

TASK [lua_env : modify /etc/yum.repos.d/CentOS-Debuginfo.repo] ***************************************************************************************************************************************************
 [WARNING]: Consider using template or lineinfile module rather than running sed

changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [lua_env : install package] *********************************************************************************************************************************************************************************
ok: [192.168.1.188] => (item=[u'gcc', u'gcc-c++', u'rpm-build', u'net-tools', u'vim', u'unzip', u'autoconf', u'git', u'automake', u'libtool', u'make', u'lsb', u'libtermcap-devel', u'ncurses-devel', u'libevent-devel', u'readline-devel', u'openssl-devel.x86_64', u'pcre-devel.x86_64', u'gperf', u'protobuf-lite-devel.x86_64', u'e2fsprogs-devel', u'libuuid-devel', u'boost-devel', u'elfutils-devel'])
ok: [192.168.1.189] => (item=[u'gcc', u'gcc-c++', u'rpm-build', u'net-tools', u'vim', u'unzip', u'autoconf', u'git', u'automake', u'libtool', u'make', u'lsb', u'libtermcap-devel', u'ncurses-devel', u'libevent-devel', u'readline-devel', u'openssl-devel.x86_64', u'pcre-devel.x86_64', u'gperf', u'protobuf-lite-devel.x86_64', u'e2fsprogs-devel', u'libuuid-devel', u'boost-devel', u'elfutils-devel'])
ok: [192.168.1.190] => (item=[u'gcc', u'gcc-c++', u'rpm-build', u'net-tools', u'vim', u'unzip', u'autoconf', u'git', u'automake', u'libtool', u'make', u'lsb', u'libtermcap-devel', u'ncurses-devel', u'libevent-devel', u'readline-devel', u'openssl-devel.x86_64', u'pcre-devel.x86_64', u'gperf', u'protobuf-lite-devel.x86_64', u'e2fsprogs-devel', u'libuuid-devel', u'boost-devel', u'elfutils-devel'])

TASK [lua_env : mkdir /data/soft/02_env_install] *****************************************************************************************************************************************************************
ok: [192.168.1.189]
ok: [192.168.1.190]
ok: [192.168.1.188]

TASK [lua_env : copy file] ***************************************************************************************************************************************************************************************
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openssl-1.0.2e.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openssl-1.0.2e.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openssl-1.0.2e.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openresty-1.11.2.3.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openresty-1.11.2.3.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'openresty-1.11.2.3.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'luarocks-2.4.1.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'luarocks-2.4.1.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'lua-5.1.5.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'lua-5.1.5.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'luarocks-2.4.1.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'drizzle7-2011.07.21.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'drizzle7-2011.07.21.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'lua-5.1.5.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'curl-7.59.0.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'curl-7.59.0.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'drizzle7-2011.07.21.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'Lua-cURLv3.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'Lua-cURLv3.tar.gz'})
changed: [192.168.1.190] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'systemtap-3.2.tar.gz'})
changed: [192.168.1.189] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'systemtap-3.2.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'curl-7.59.0.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'Lua-cURLv3.tar.gz'})
changed: [192.168.1.188] => (item={u'dest': u'/data/soft/02_env_install', u'src': u'systemtap-3.2.tar.gz'})

TASK [lua_env : install lua-5.1.5.tar.gz] ************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : install luarocks-2.4.1.tar.gz] *******************************************************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]
changed: [192.168.1.188]

TASK [lua_env : install drizzle7-2011.07.21.tar.gz] **************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : link libdrizzle] *********************************************************************************************************************************************************************************
changed: [192.168.1.189]
ok: [192.168.1.190]
ok: [192.168.1.188]

TASK [lua_env : link libdrizzle64] *******************************************************************************************************************************************************************************
changed: [192.168.1.189]
ok: [192.168.1.190]
ok: [192.168.1.188]

TASK [lua_env : unzip openssl-1.0.2e.tar.gz] *********************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : install openresty-1.11.2.3.tar.gz] ***************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : luarocks install package] ************************************************************************************************************************************************************************
changed: [192.168.1.189] => (item=luafilesystem)
changed: [192.168.1.190] => (item=luafilesystem)
changed: [192.168.1.188] => (item=luafilesystem)
changed: [192.168.1.190] => (item=luabitop)
changed: [192.168.1.189] => (item=luabitop)
changed: [192.168.1.188] => (item=luabitop)
changed: [192.168.1.190] => (item=lua-resty-template)
changed: [192.168.1.189] => (item=lua-resty-template)
changed: [192.168.1.188] => (item=lua-resty-template)
changed: [192.168.1.190] => (item=lua-resty-session)
changed: [192.168.1.189] => (item=lua-resty-session)
changed: [192.168.1.188] => (item=lua-resty-session)
changed: [192.168.1.190] => (item=luacrypto)
changed: [192.168.1.189] => (item=luacrypto)
changed: [192.168.1.188] => (item=luacrypto)

TASK [lua_env : install curl-7.59.0.tar.gz] **********************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : install Lua-cURLv3.tar.gz] ***********************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : luarocks install packages] ***********************************************************************************************************************************************************************
changed: [192.168.1.189] => (item=luaossl)
changed: [192.168.1.190] => (item=luaossl)
changed: [192.168.1.188] => (item=luaossl)
changed: [192.168.1.189] => (item=lua-path)
changed: [192.168.1.190] => (item=lua-path)
changed: [192.168.1.188] => (item=lua-path)
changed: [192.168.1.189] => (item=xml)
changed: [192.168.1.190] => (item=xml)
changed: [192.168.1.188] => (item=xml)
changed: [192.168.1.190] => (item=snowflake)
changed: [192.168.1.189] => (item=snowflake)
changed: [192.168.1.188] => (item=snowflake)
changed: [192.168.1.190] => (item=luasocket)
changed: [192.168.1.189] => (item=luasocket)
changed: [192.168.1.188] => (item=luasocket)
changed: [192.168.1.190] => (item=lua-resty-http)
changed: [192.168.1.189] => (item=lua-resty-http)
changed: [192.168.1.188] => (item=lua-resty-http)
changed: [192.168.1.190] => (item=lua-resty-jit-uuid)
changed: [192.168.1.189] => (item=lua-resty-jit-uuid)
changed: [192.168.1.188] => (item=lua-resty-jit-uuid)

TASK [lua_env : install systemtap] *******************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : tar xf FlameGraph.tar.gz -C /opt] ****************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : tar xf openresty-systemtap-toolkit.tar.gz -C /opt] ***********************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

TASK [lua_env : copy file] ***************************************************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]
changed: [192.168.1.188]

PLAY RECAP *******************************************************************************************************************************************************************************************************
192.168.1.188              : ok=21   changed=15   unreachable=0    failed=0   
192.168.1.189              : ok=21   changed=17   unreachable=0    failed=0   
192.168.1.190              : ok=21   changed=15   unreachable=0    failed=0
```



**验证lua模块**

```shell
[root@node2 ~]# luarocks list

Installed rocks:
----------------

lua-curl
   0.3.7-1 (installed) - /usr/local/lib/luarocks/rocks

lua-path
   0.3.0-2 (installed) - /usr/local/lib/luarocks/rocks

lua-resty-http
   0.12-0 (installed) - /usr/local/lib/luarocks/rocks

lua-resty-jit-uuid
   0.0.7-1 (installed) - /usr/local/lib/luarocks/rocks

lua-resty-session
   2.22-1 (installed) - /usr/local/lib/luarocks/rocks

lua-resty-template
   1.9-1 (installed) - /usr/local/lib/luarocks/rocks

luabitop
   1.0.2-3 (installed) - /usr/local/lib/luarocks/rocks

luacrypto
   0.3.2-2 (installed) - /usr/local/lib/luarocks/rocks

luafilesystem
   1.7.0-2 (installed) - /usr/local/lib/luarocks/rocks

luaossl
   20180530-0 (installed) - /usr/local/lib/luarocks/rocks

luasocket
   3.0rc1-2 (installed) - /usr/local/lib/luarocks/rocks

lub
   1.1.0-1 (installed) - /usr/local/lib/luarocks/rocks

snowflake
   1.0-1 (installed) - /usr/local/lib/luarocks/rocks

xml
   1.1.3-1 (installed) - /usr/local/lib/luarocks/rocks
   
[root@node2 ~]# stap -ve 'probe begin { log("hello systemtap!") exit() }'
Pass 1: parsed user script and 471 library scripts using 239840virt/41772res/3420shr/38384data kb, in 340usr/20sys/537real ms.
Pass 2: analyzed script: 1 probe, 2 functions, 0 embeds, 0 globals using 240764virt/42824res/3604shr/39308data kb, in 10usr/0sys/5real ms.
Pass 3: translated to C into "/tmp/stapIOSHDI/stap_6b8b1c2d3f335bedf8901d5e4414ec9f_1107_src.c" using 240764virt/43420res/4168shr/39308data kb, in 0usr/0sys/0real ms.
Pass 4: compiled C into "stap_6b8b1c2d3f335bedf8901d5e4414ec9f_1107.ko" in 7740usr/1900sys/9782real ms.
Pass 5: starting run.
hello systemtap!
Pass 5: run completed in 0usr/30sys/341real ms.
```







