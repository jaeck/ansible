# 部署Mongodb集群

## 环境准备

| hostname |      ip       | 应用                                                         |
| :------: | :-----------: | :----------------------------------------------------------- |
|  node1   | 192.168.1.188 | mongos/config server/副本集1主节点/副本集2仲裁/副本集3从节点 |
|  node2   | 192.168.1.189 | mongos/config server/副本集1从节点/副本集2主节点/副本集3仲裁 |
|  node3   | 192.168.1.190 | mongos/config server/副本集1仲裁/副本集2从节点/副本集3主节点 |

- **端口分配：mongos 20000、config 21000、副本集1 27001、副本集2 27002、副本集3 27003**


- **三台机器全部关闭firewalld服务和selinux，或者增加对应端口的规则**





**注意： 采用ansible一键部署Mongodb集群**

* 系统环境：Centos7 x64
* 各应用安装目录 /data/app
* 纯净系统



## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# git clone http://222.190.240.138:8000/document/server-conf.git
[root@node1 ~]# rm -rf /etc/ansible/*
[root@node1 ~]# mv server-conf/* /etc/ansible
[root@node1 server-conf]# ls /etc/ansible/
ansible.cfg  codis-cluster-Readme.md  hosts  kafka-cluster-Readme.md  mongodb-cluster-Readme.md  roles  site.yaml
```

**配置hosts**

```shell
[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
192.168.1.190 node3
```

**配置/etc/ansbile**

* 把192.168.1.188/189/190 替换成当前的ip地址**(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
[root@node1 ~]# vim /etc/ansible/hosts
[mongodb]
192.168.1.188 local_ip=192.168.1.188
192.168.1.189 local_ip=192.168.1.189
192.168.1.190 local_ip=192.168.1.190

[mongodb:vars]
app_dir=/data/app/mongodb
conf_dir=/etc/mongod
mongos_port=20000
mongos_log=/tmp/mongos.log

config_port=21000
config_name=configs
config_log=/tmp/conf.log

shard1_name=shard1
shard2_name=shard2
shard3_name=shard3
shard1_port=27001
shard2_port=27002
shard3_port=27003
shard_log=/tmp/shard.log

mongodb1_ip=192.168.1.188
mongodb2_ip=192.168.1.189
mongodb3_ip=192.168.1.190

```

**配置ssh免秘钥认证, 在node1上操作.**

```shell
# 运行命令, 直接回车三次即可. 
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到三个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189
[root@node1 ~]# ssh-copy-id root@192.168.1.190

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.
[root@node1 ~]# ssh root@192.168.1.190
Last login: Fri May  4 14:00:47 2018 from 192.168.1.190
[root@node3 ~]# exit
logout
Connection to 192.168.1.190 closed.
```

**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ansible]# pwd
/etc/ansible
[root@node1 ansible]# more site.yaml 
- hosts: mongodb
  remote_user: root
  roles:
  - common
  - mongodb
  
# 首先测试下语法是否有问题
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check

playbook: site.yaml

# 然后开始运行
[root@node1 ansible]# ansible-playbook site.yaml

# 没有失败代表没问题，最后一步就是验证.
```

## 验证

```shell
#登陆任何一台机器2000端口测试

# mongo --host 192.168.1.188 --port 20000
> sh.status()	
```







