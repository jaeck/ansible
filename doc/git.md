# git常用命令



### git基础命令：

```
git log --oneline --decorate --all --graph    树状显示

git status

git push  上传代码

git pull    拉取代码

git log HEAD -1 --stat -p   查看详细log信息

git show  查看log信息

git commit --amend   增补修改，然后合并最后一次提交

git log --oneline  将每一个历史提交信息打印成一行显示在屏幕上.

git branch ansible0614  创建分支

git checkout -b ansible0614 快速创建和进入分支

git branch -D ansible 0614  删除分支

git blame README.md  查看README.md文件具体提交的信息

```



what is HEAD?

```shell
# HEAD指向refs/heads/master文件
[root@node1 git-practice]# cat .git/HEAD 
ref: refs/heads/master

# 这个master文件其实就是一个40位的哈希值, 和上面的哈希值一样，所以效果一样.
[root@node1 git-practice]# cat .git/refs/heads/master 
e8ea9a476a61f3a33b6fcae4cda5b784381bb732
```



###  git 场景操作：

```shell
合并分支，必须切回master.
1. 查看分支差异               git log master..ansible0614
2. 合并分支                   git mergr ansible0614
3. 解决冲突                   git status  git diff  
4. 然后单独修改文件，最后    	git add .  和  git commit 

合并单个commit
1. 树状形式查看           	git log --oneline --decorate --all --graph
2. 合并单个commit     	 	  git cherry-pick 5e8ed96
```

