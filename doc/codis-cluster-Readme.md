# 部署codis集群

## 环境准备

| hostname |      ip       | 应用                                                         |     系统      |
| :------: | :-----------: | :----------------------------------------------------------- | :-----------: |
|  node1   | 192.168.1.188 | jdk zookeeper redis-master:6381 redis-slave:6382 codis-proxy codis-dashboard codis-fe | Centos7.3 x64 |
|  node2   | 192.168.1.189 | jdk zookeeper redis-master:6381 redis-slave:6382 codis-proxy | Centos7.3 x64 |
|  node3   | 192.168.1.190 | jdk zookeeper redis-master:6381 redis-slave:6382 codis-proxy | Centos7.3 x64 |

**注意： 采用ansible一键部署codis集群(ansible_v4.tar.gz)**

* 系统环境：Centos7 x64
* 各应用安装目录 /data/app
* 纯净系统



## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# git clone http://222.190.240.138:8000/document/server-conf.git
[root@node1 ~]# rm -rf /etc/ansible/*
[root@node1 ~]# mv server-conf/* /etc/ansible
[root@node1 server-conf]# ls /etc/ansible/
ansible.cfg  codis-cluster-Readme.md  hosts  kafka-cluster-Readme.md  mongodb-cluster-Readme.md  roles  site.yaml
```

**配置hosts**

```shell
# codis proxy需要靠主机名来通讯, 此处需要配置好, 否则后期会出问题.
[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
192.168.1.190 node3
```

**配置/etc/ansbile**

* 把192.168.1.188/189/190 替换成当前的ip地址**(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
[root@node1 ~]# vim /etc/ansible/hosts
[codis]
192.168.1.188  myid=1 redis_master_ip=192.168.1.188
192.168.1.189  myid=2 redis_master_ip=192.168.1.189
192.168.1.190  myid=3 redis_master_ip=192.168.1.190

[codis:vars]
server1=192.168.1.188
server2=192.168.1.189
server3=192.168.1.190
app_dir=/data/app

zk_port=2181
zk_package=zookeeper-3.4.6.tar.gz
zk=zookeeper-3.4.6
zk_link=/data/app/zookeeper

jdk_package=jdk-8u151-linux-x64.tar.gz
jdk=jdk1.8.0_151
java_env=/data/app/jdk

go_package=go1.7.6.linux-amd64.tar.gz
go_unzip_dir=go
go_github_dir=/data/app/codis/src/github.com/

codis_package=bin.tar.gz

redis_master_port=6381
redis_slave_port=6382
redis_require=codispass

redis_sentinel_port=26381

codis_path=/data/app/codis
codis_conf=/data/app/codis/conf
codis_log=/data/app/codis/logs
codis_bin=/data/app/codis/bin
codis_data=/data/app/codis/data
codis_run=/data/app/codis/run

[dashboard-proxy-fe]
192.168.1.188

[dashboard-proxy-fe:vars]
server1=192.168.1.188
server2=192.168.1.189
server3=192.168.1.190
codis_project_name=codis_project
redis_require=codispass
proxy_ip=0.0.0.0
proxy_real_ip=192.168.1.188
codis_conf=/data/app/codis/conf
codis_bin=/data/app/codis/bin
codis_log=/data/app/codis/logs

[add-codis-proxy]
192.168.1.189 proxy_real_ip=192.168.1.189
192.168.1.190 proxy_real_ip=192.168.1.190

[add-codis-proxy:vars]
server1=192.168.1.188
server2=192.168.1.189
server3=192.168.1.190
codis_project_name=codis_project
redis_require=codispass
proxy_ip=0.0.0.0
codis_conf=/data/app/codis/conf
codis_bin=/data/app/codis/bin
codis_log=/data/app/codis/logs
```

**配置ssh免秘钥认证, 在node1上操作.**

```shell
# 运行命令, 直接回车三次即可. 
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到三个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189
[root@node1 ~]# ssh-copy-id root@192.168.1.190

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.
[root@node1 ~]# ssh root@192.168.1.190
Last login: Fri May  4 14:00:47 2018 from 192.168.1.190
[root@node3 ~]# exit
logout
Connection to 192.168.1.190 closed.
```

**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ~]# cd /etc/ansbile/

# 检测语法
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check

# 开始安装
[root@node1 ansible]# ansible-playbook site.yaml 

PLAY RECAP ****************************************************************************************************************************************************************
192.168.1.188               : ok=73   changed=68   unreachable=0    failed=0   
192.168.1.189               : ok=67   changed=62   unreachable=0    failed=0   
192.168.1.190               : ok=67   changed=62   unreachable=0    failed=0

# failed=0 代表没有错误

# 查看相关端口是否启动, 各个节点
[root@node1 ansible]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address  Foreign Address State  PID/Program name    
tcp     0      0 127.0.0.1:6381      0.0.0.0:*       LISTEN      89186/codis-server  
tcp     0      0 192.168.1.188:6381   0.0.0.0:*      LISTEN      89186/codis-server  
tcp     0      0 0.0.0.0:26381       0.0.0.0:*       LISTEN      89101/redis-sentine 
tcp     0      0 127.0.0.1:6382       0.0.0.0:*      LISTEN      89272/codis-server  
tcp     0      0 192.168.1.188:6382    0.0.0.0:*     LISTEN      89272/codis-server  
tcp     0      0 192.168.1.188:3888    0.0.0.0:*     LISTEN      86342/java          
tcp     0      0 0.0.0.0:22           0.0.0.0:*      LISTEN      83328/sshd          
tcp     0      0 0.0.0.0:19000        0.0.0.0:*      LISTEN      90106/codis-proxy   
tcp     0      0 0.0.0.0:38592        0.0.0.0:*      LISTEN      86342/java     
tcp     0      0 0.0.0.0:2181         0.0.0.0:*      LISTEN      86342/java          
tcp6    0      0 :::26381             :::*           LISTEN      89101/redis-sentine 
tcp6    0      0 :::8080              :::*           LISTEN      90487/codis-fe      
tcp6    0      0 :::22                :::*           LISTEN      83328/sshd          
tcp6    0      0 :::18080             :::*           LISTEN      89731/codis-dashboa 
tcp6    0      0 :::11080             :::*           LISTEN      90106/codis-proxy  

[root@node2 ~]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address  Foreign Address  State   PID/Program name    
tcp     0      0 127.0.0.1:6381     0.0.0.0:*     LISTEN      79915/codis-server  
tcp     0      0 192.168.1.189:6381  0.0.0.0:*    LISTEN      79915/codis-server  
tcp     0      0 0.0.0.0:26381      0.0.0.0:*     LISTEN      79857/redis-sentine 
tcp     0      0 127.0.0.1:6382     0.0.0.0:*     LISTEN      79973/codis-server  
tcp     0      0 192.168.1.189:6382  0.0.0.0:*    LISTEN      79973/codis-server  
tcp     0      0 192.168.1.189:3888  0.0.0.0:*    LISTEN      77967/java          
tcp     0      0 0.0.0.0:22         0.0.0.0:*     LISTEN      75942/sshd          
tcp     0      0 0.0.0.0:19000      0.0.0.0:*     LISTEN      80363/codis-proxy   
tcp     0      0 0.0.0.0:2181       0.0.0.0:*     LISTEN      77967/java          
tcp     0      0 0.0.0.0:43527      0.0.0.0:*     LISTEN      77967/java          
tcp6    0      0 :::26381           :::*          LISTEN      79857/redis-sentine 
tcp6    0      0 :::22              :::*          LISTEN      75942/sshd          
tcp6    0      0 :::11080           :::*          LISTEN      80363/codis-proxy

[root@node3 ~]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address  Foreign Address  State   PID/Program name    
tcp     0      0 127.0.0.1:6381     0.0.0.0:*     LISTEN      79915/codis-server  
tcp     0      0 192.168.1.190:6381  0.0.0.0:*     LISTEN      79915/codis-server  
tcp     0      0 0.0.0.0:26381      0.0.0.0:*     LISTEN      79857/redis-sentine 
tcp     0      0 127.0.0.1:6382     0.0.0.0:*     LISTEN      79973/codis-server  
tcp     0      0 192.168.1.190:6382  0.0.0.0:*     LISTEN      79973/codis-server  
tcp     0      0 192.168.1.190:3888  0.0.0.0:*     LISTEN      77967/java          
tcp     0      0 0.0.0.0:22         0.0.0.0:*     LISTEN      75942/sshd          
tcp     0      0 0.0.0.0:19000      0.0.0.0:*     LISTEN      80363/codis-proxy   
tcp     0      0 0.0.0.0:2181       0.0.0.0:*     LISTEN      77967/java          
tcp     0      0 0.0.0.0:43527      0.0.0.0:*     LISTEN      77967/java          
tcp6    0      0 :::26381           :::*          LISTEN      79857/redis-sentine 
tcp6    0      0 :::22              :::*          LISTEN      75942/sshd          
tcp6    0      0 :::11080           :::*          LISTEN      80363/codis-proxy
```

## 配置dashboard

**打开浏览器访问 http://192.168.1.188:8080**

**点击下当前项目 codis_project, 并且添加下proxy节点.**

**默认界面没有添加proxy节点, 三台机器日志如下：**

```shell
[root@node1 ansible]# tailf /data/app/codis/logs/codis/proxy.log.2018-05-11 
2018/05/11 11:36:03 main.go:229: WARN proxy waiting online ...
2018/05/11 11:36:04 main.go:229: WARN proxy waiting online ...
2018/05/11 11:36:05 main.go:229: WARN proxy waiting online ...

[root@node2 app]# tailf /data/app/codis/logs/codis/proxy.log.2018-05-11 
2018/05/11 11:36:01 main.go:229: WARN proxy waiting online ...
2018/05/11 11:36:02 main.go:229: WARN proxy waiting online ...
2018/05/11 11:36:03 main.go:229: WARN proxy waiting online ...
2018/05/11 11:36:04 main.go:229: WARN proxy waiting online ...

[root@node3 ~]# tailf /data/app/codis/logs/codis/proxy.log.2018-05-11 
2018/05/11 11:35:53 main.go:229: WARN proxy waiting online ...
2018/05/11 11:35:54 main.go:229: WARN proxy waiting online ...
2018/05/11 11:35:55 main.go:229: WARN proxy waiting online ...
```

然后我们需要在fe界面把proxy添加进去, 日志都会有下面的提示：(提示代表proxy已经开始工作)

```shell
2018/05/11 11:36:55 proxy_api.go:44: WARN API call /api/proxy/start/13c708b6ef6686249f81b87d269c0e2c from 192.168.1.188:47994 []
2018/05/11 11:36:55 proxy_api.go:44: WARN API call /api/proxy/sentinels/13c708b6ef6686249f81b87d269c0e2c from 192.168.1.188:47994 []
2018/05/11 11:36:55 proxy.go:293: WARN set sentinels = []
2018/05/11 11:36:55 jodis.go:95: [WARN] jodis create node /jodis/codis_project/proxy-b054d1f747de7c93cc2c5ff29c5302ac
2018/05/11 11:36:56 main.go:233: WARN proxy is working ...
```


![Alt text](http://p68js8hy6.bkt.clouddn.com/codis.png "optional title")

**添加proxy**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_proxy.png "optional title")

**添加redis group**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_group.png "optional title")

**添加redis节点(主从都要添加)**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_redis.png "optional title")

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_redis1.png "optional title")

**添加sentinel节点**

**把sentinel节点ip和端口写好，点击Add Sentinel 即可.**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_sentinel.png "optional title")

**添加好sentinel之后, 手动点击下SYNC.**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_sentinel1.png "optional title")

**点击ok, 此操作会自动配置sentinel.conf。(点击OK的同时, 会写入sentinel.conf, 自动配置sentinel.)**

![Alt text](http://p68js8hy6.bkt.clouddn.com/add_sentinel2.png "optional title")

**自动分片和手动分片**

![Alt text](http://p68js8hy6.bkt.clouddn.com/codis-slots.png "optional title")



## 测试codis集群



* 测试node3, redis-master节点down

```shell
[root@node3 ~]# systemctl stop redis6381

# 通过sentinel切换的时间大约为5秒左右, 时间应该可以缩小。(后面优化配置文件)
[root@node3 ~]# redis-cli -p 6382
127.0.0.1:6382> info replication
# Replication
role:master
connected_slaves:0
master_repl_offset:0
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

**以上主从切换是没有问题, 但是会有延迟的时间, 后续继续优化配置文件.**

但在codis-fe界面, 还是会有错误.

![img](http://p68js8hy6.bkt.clouddn.com/codis-4.png )

**现在重新启动下redis6381, 看看界面是否会发生改变.**

![img](http://p68js8hy6.bkt.clouddn.com/codis-2.png )

**即使恢复正常, 还是会有提示(OUT OF SYNC), 只需要点击下SYNC即可回复界面的正常.**

![img](http://p68js8hy6.bkt.clouddn.com/codis-3.png )



* proxy压力测试

  **proxy端口为19000. 在node1, node2, node3 节点都已经启动, 连接任何节点的19000端口都可以.**

  **redis密码和codis集群的密码都是 codispass**



​	redis自带压测工具redis-benchmark

​	[root@node3 ~]# redis-benchmark -h 192.168.1.188 -p 19000 -a codispass -t set,get -n 100000 -q
​	SET: 68587.11 requests per second
​	GET: 77519.38 requests per second



​	**如果需要代码连接codis集群，可以连接任意一台proxy节点的19000端口， 密码是 codispass**

​	







