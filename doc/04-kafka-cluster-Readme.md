# 部署Kafka集群

## 环境准备

|     hostname     |      ip       | 应用            |      系统      |
| :--------------: | :-----------: | :-------------- | :------------: |
| node1（ansible） | 192.168.1.188 | zookeeper kafka | Centos7.4 1804 |
|      node2       | 192.168.1.189 | zookeeper kafka | Centos7.4 1804 |
|      node3       | 192.168.1.189 | zookeeper kafka | Centos7.4 1804 |

**注意： 采用ansible一键部署kafka集群**

* 系统环境：Centos7.4 1804
* 各应用安装目录 /data/app
* 纯净系统



## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# git clone ssh://git@69.172.89.221:18822/document/server-conf.git
[root@node1 ~]# rm -rf /etc/ansible/*
[root@node1 ~]# mv server-conf/* /etc/ansible
[root@node1 server-conf]# ls /etc/ansible/
ansible.cfg  doc  hosts  roles  site.yaml
```



**配置ssh免秘钥认证, 在node1上操作.**

```shell
# 运行命令, 直接回车三次即可. 如果原来已经有，可以不生成秘钥。
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到三个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189
[root@node1 ~]# ssh-copy-id root@192.168.1.190

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.

[root@node1 ~]# ssh root@192.168.1.190
Last login: Fri May  4 14:00:45 2018 from 192.168.1.190
[root@node2 ~]# exit
logout
Connection to 192.168.1.190 closed.
```



**配置hosts**

```shell
# 192.168.1.188/189/190 修改hostname
[root@node1 ~]# hostnamectl set-hostname node1
[root@node2 ~]# hostnamectl set-hostname node2
[root@node3 ~]# hostnamectl set-hostname node3

[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
192.168.1.190 node3
```



**配置/etc/ansbile**

* 把192.168.1.188/189/190 替换成当前的ip地址**(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
# 必须要安装jdk，jdk在基础环境已经配置好了。
# ssh端口默认22, 如果端口修改成18822，请在ip后面加上 ansible_ssh_port=18822 (和ip之前须有空格)

[root@node1 ~]# vim /etc/ansible/hosts
[kafka]
192.168.1.188 broker_id=1 myid=1 local_ip=192.168.1.188
192.168.1.189 broker_id=2 myid=2 local_ip=192.168.1.189
192.168.1.190 broker_id=3 myid=3 local_ip=192.168.1.190

[kafka:vars]
app_dir=/data/app
kafka_link=/data/app/kafka
kafka_logs=/data/app/kafka/logs
kafka_package=kafka_2.9.2-0.8.1.tgz
kafka=kafka_2.9.2-0.8.1

server1=192.168.1.188
server2=192.168.1.189
server3=192.168.1.190

zk_port=2181
zk_package=zookeeper-3.4.6.tar.gz
zk=zookeeper-3.4.6
zk_link=/data/app/zookeeper

java_env=/data/app/jdk
```



**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ansbile]# pwd
/etc/ansible

# 必须要安装基础环境的jdk，否则安装失败。
[root@node1 ansible]# more site.yaml 
- hosts: kafka
  remote_user: root
  roles:
  - zookeeper
  - kafka
  
# 首先测试下语法是否有问题
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check

playbook: site.yaml

# 然后开始运行
[root@node1 ansible]# ansible-playbook site.yaml

# 没有失败代表没问题，最后一步就是验证.
```

## 验证

```shell
# 登录机器首先要 source /etc/profile
[root@node1 ~]# source /etc/profile

# zookeeper 端口 2181    kafka 端口 9092
[root@node1 ~]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address   Foreign Address    State   PID/Program name    
tcp    0     0 192.168.1.188:3888      0.0.0.0:*        LISTEN      17146/java          
tcp    0      0 0.0.0.0:22             0.0.0.0:*        LISTEN      10110/sshd          
tcp    0      0 0.0.0.0:33596          0.0.0.0:*        LISTEN      19054/java          
tcp    0      0 0.0.0.0:35997          0.0.0.0:*        LISTEN      17146/java          
tcp    0      0 0.0.0.0:10050          0.0.0.0:*        LISTEN      1929/zabbix_agentd  
tcp    0      0 192.168.1.188:9092     0.0.0.0:*        LISTEN      19054/java          
tcp    0      0 0.0.0.0:2181           0.0.0.0:*        LISTEN      17146/java          
tcp    0      0 127.0.0.1:8005         0.0.0.0:*        LISTEN      30244/java          
tcp6   0      0 :::22                  :::*             LISTEN      10110/sshd  

# 验证zookeeper是否启动
[root@node1 ~]# zkServer.sh status
JMX enabled by default
Using config: /data/app/zookeeper/bin/../conf/zoo.cfg
Mode: follower

[root@node2 ~]# zkServer.sh status
JMX enabled by default
Using config: /data/app/zookeeper/bin/../conf/zoo.cfg
Mode: follower

[root@node3 ~]# zkServer.sh status
JMX enabled by default
Using config: /data/app/zookeeper/bin/../conf/zoo.cfg
Mode: leader

# 验证kafka集群, 创建Topic
[root@node1 ~]# kafka-topics.sh --create --zookeeper 192.168.1.188 --replication-factor 2 --partitions 1 --topic test
Created topic "test".

# 在一台服务器上创建一个发布者
[root@node1 ~]# kafka-console-producer.sh --broker-list 192.168.1.188:9092 --topic test
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.


# 在一台服务器上创建一个订阅者
[root@node2 ~]# kafka-console-consumer.sh --zookeeper 192.168.1.189:2181 --topic test --from-beginning
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.

# 在发布者中输入什么， 订阅者就可以收到什么，即测试成功.
[root@node1 ~]# kafka-console-producer.sh --broker-list 192.168.1.188:9092 --topic test
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
s
z

c
v
s
a
d

[root@node2 ~]# kafka-console-consumer.sh --zookeeper 192.168.1.189:2181 --topic test --from-beginning
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
s
z

c
v
s
a
d

```







