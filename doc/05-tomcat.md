# 部署tomcat

## 环境准备

|     hostname     |      ip       | 应用            |      系统      |
| :--------------: | :-----------: | :-------------- | :------------: |
| node1（ansible） | 192.168.1.188 | zookeeper kafka | Centos7.4 1804 |
|      node2       | 192.168.1.189 | zookeeper kafka | Centos7.4 1804 |
|      node3       | 192.168.1.189 | zookeeper kafka | Centos7.4 1804 |

**注意： 采用ansible一键部署tomcat**

* 系统环境：Centos7.4 1804
* 各应用安装目录 /data/app
* 纯净系统



## 安装步骤(在node1上操作)

**在node1节点上安装ansbile管理工具**

```shell
[root@node1 ~]# yum install epel-release -y
[root@node1 ~]# yum install git ansible -y
[root@node1 ~]# git clone ssh://git@69.172.89.221:18822/document/server-conf.git
[root@node1 ~]# rm -rf /etc/ansible/*
[root@node1 ~]# mv server-conf/* /etc/ansible
[root@node1 server-conf]# ls /etc/ansible/
ansible.cfg  doc  hosts  roles  site.yaml
```



**配置ssh免秘钥认证, 在node1上操作.**

```shell
# 运行命令, 直接回车三次即可. 如果原来已经有，可以不生成秘钥。
[root@node1 ~]# yum -y install openssh-clients
[root@node1 ~]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/07zV2kerX4Pwzsupny6D8Fqr1Zv62HM4HGiHKb62tw root@node3
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         .       |
|        S * .   o|
|       + *.O . +o|
|      . =.+.B *.o|
|     + o.o =*+o=o|
|    oo+.E.*@*==+o|
+----[SHA256]-----+

# 把ssh的公钥拷贝到三个节点上, 当前以188 189 190为例.
[root@node1 ~]# ssh-copy-id root@192.168.1.188
[root@node1 ~]# ssh-copy-id root@192.168.1.189
[root@node1 ~]# ssh-copy-id root@192.168.1.190

# 测试ssh
[root@node1 ~]# ssh root@192.168.1.188
Last login: Fri May  4 14:00:42 2018 from 192.168.1.188
[root@node1 ~]# exit
logout
Connection to 192.168.1.188 closed.
[root@node1 ~]# ssh root@192.168.1.189
Last login: Fri May  4 14:00:45 2018 from 192.168.1.189
[root@node2 ~]# exit
logout
Connection to 192.168.1.189 closed.

[root@node1 ~]# ssh root@192.168.1.190
Last login: Fri May  4 14:00:45 2018 from 192.168.1.190
[root@node2 ~]# exit
logout
Connection to 192.168.1.190 closed.
```



**配置hosts**

```shell
# 192.168.1.188/189/190 修改hostname
[root@node1 ~]# hostnamectl set-hostname node1
[root@node2 ~]# hostnamectl set-hostname node2
[root@node3 ~]# hostnamectl set-hostname node3

[root@node1 roles]# vim /etc/ansible/roles/common/files/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.188 node1
192.168.1.189 node2
192.168.1.190 node3
```



**配置/etc/ansbile**

* 把192.168.1.188/189/190 替换成当前的ip地址**(此步骤配置错误, 将安装报错. ip地址批量替换即可)**

```shell
# 必须要安装jdk，jdk在基础环境已经配置好了。
# ssh端口默认22, 如果端口修改成18822，请在ip后面加上 ansible_ssh_port=18822 (和ip之前须有空格)

[root@node1 ~]# vim /etc/ansible/hosts
[tomcat]
192.168.1.189
192.168.1.190
```



**开始安装**

```shell
# 测试没问题, 现在可以开始安装.
[root@node1 ansbile]# pwd
/etc/ansible

# 必须要安装基础环境的jdk，否则安装失败。
[root@node1 ansible]# more site.yaml 
- hosts: tomcat 
  remote_user: root
  roles:
  - tomcat
  
# 首先测试下语法是否有问题
[root@node1 ansible]# ansible-playbook site.yaml --syntax-check

playbook: site.yaml

# 然后开始运行
[root@node1 ansible]# ansible-playbook site.yaml

# 没有失败代表没问题，最后一步就是验证.
```

## 验证

```shell
[root@node1 ansible]# ansible-playbook site.yaml 

PLAY [tomcat] *********************************************************************************************************************************************************************

TASK [Gathering Facts] ************************************************************************************************************************************************************
ok: [192.168.1.189]
ok: [192.168.1.190]

TASK [tomcat : mkdir /data/app] ***************************************************************************************************************************************************
ok: [192.168.1.190]
ok: [192.168.1.189]

TASK [tomcat : tar xf apache-tomcat-8.0.26.tar.gz] ********************************************************************************************************************************
changed: [192.168.1.189]
changed: [192.168.1.190]

TASK [tomcat : config tomcat] *****************************************************************************************************************************************************
changed: [192.168.1.190]
changed: [192.168.1.189]

PLAY RECAP ************************************************************************************************************************************************************************
192.168.1.189              : ok=4    changed=2    unreachable=0    failed=0   
192.168.1.190              : ok=4    changed=2    unreachable=0    failed=0

# 最后只需要把war包放在webapps下即可.
```







