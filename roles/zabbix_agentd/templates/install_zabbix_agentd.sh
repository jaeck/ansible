#!/bin/bash

SERVER_IP={{ zabbix_server_ip }}
LOCAL_IP={{ local_ip }}

yum install sysstat -y

groupadd zabbix
useradd -r -g zabbix -s /sbin/nologin zabbix
yum install net-snmp-devel -y

cd /data/soft && rm -rf zabbix-3.2.11; tar xf zabbix-3.2.11.tar.gz && cd zabbix-3.2.11 && ./configure --prefix=/data/app/zabbix --enable-agent && make && make install

mkdir /var/log/zabbix
chown zabbix.zabbix /var/log/zabbix

cd /data/soft/zabbix-3.2.11 && \cp misc/init.d/fedora/core/zabbix_agentd /etc/init.d
chmod 755 /etc/init.d/zabbix_agentd

ln -s /data/app/zabbix/etc/ /etc/zabbix
ln -s /data/app/zabbix/bin/ /usr/bin/
ln -s /data/app/zabbix/sbin/ /usr/sbin/

sed -i "s@BASEDIR=/usr/local@BASEDIR=/data/app/zabbix@g" /etc/init.d/zabbix_agentd
sed -i "s@Server=127.0.0.1@Server=$SERVER_IP@g" /etc/zabbix/zabbix_agentd.conf
sed -i "s@ServerActive=127.0.0.1@ServerActive=$SERVER_IP:10051@g" /etc/zabbix/zabbix_agentd.conf
sed -i "s@tmp/zabbix_agentd.log@var/log/zabbix/zabbix_agentd.log@g"  /etc/zabbix/zabbix_agentd.conf
sed -i "s@^# UnsafeUserParameters=0@UnsafeUserParameters=1\n@g" /etc/zabbix/zabbix_agentd.conf
sed -i "s@Hostname=Zabbix server@Hostname=$LOCAL_IP@g" /etc/zabbix/zabbix_agentd.conf
sed -i "s@# Timeout=3@Timeout=30@g" /etc/zabbix/zabbix_agentd.conf

# copy sh
cd /data/soft/ && \cp disk_discovery.sh  disk_status.sh  disk_tps.sh tcp_ss_status.sh /data/app/zabbix/bin && chmod +x /data/app/zabbix/bin/*

# copy reset_tmp.sh
mkdir /data/app/sh
cd /data/soft/ && \cp reset_tmp.sh /data/app/sh/ && chmod +x /data/app/sh/*

nohup /usr/bin/iostat -dxkt 1  > /tmp/iostat_output 2>/dev/null &
nohup iostat -dtk 3 > /tmp/tps_output 2>/dev/null &

# crontab -e
echo '1 0 * * * /usr/bin/sh /data/app/sh/reset_tmp.sh >/dev/null  2>&1' >> /var/spool/cron/root

# rm zabbix
cd /data/soft/ && rm -rf zabbix-3.2.11

# setfacl -m u:zabbix:r-- /var/log/secure
setfacl -m u:zabbix:r-- /var/log/secure
setfacl -m u:zabbix:r-- /var/log/messages

chkconfig zabbix_agentd on

