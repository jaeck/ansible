#!/bin/bash
# Author: HuangXiang

contact1='whatisajava@126.com'
#contact2='zhurh@91wangjing.com'
#contact3='459575602@qq.com'

notify() {
    mailsubject="`hostname` to be $1: $vip floating"
    mailbody="`date '+%F %H:%M:%S'`: vrrp transition, `hostname` changed to be $1"
    echo $mailbody | mail -s "$mailsubject" $contact1
#    echo $mailbody | mail -s "$mailsubject" $contact2
#    echo $mailbody | mail -s "$mailsubject" $contact3
}
case "$1" in
    master)
        notify master
        exit 0
    ;;
    backup)
        notify backup
        systemctl start nginx
        exit 0
    ;;
    fault)
        notify fault
        systemctl start nginx
        exit 0
    ;;
    *)
        echo 'Usage: `basename $0` {master|backup|fault}'
        exit 1
    ;;
esac


