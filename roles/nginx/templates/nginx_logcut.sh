#!/bin/bash
#description:cut nginx log per day.

#define logs dir
LOGS_ACCESS="/data/logs/nginx/"
LOGS_ERROR="/data/logs/nginx/"

#define pid file
PID_PATH="/data/app/nginx/logs/nginx.pid"
#define date
DATE=`date -d "yesterday" +%F`
DATE_DIR=`date +%Y-%m`

#cut log
if [ ! -d ${LOGS_ACCESS}/${DATE_DIR} ];then
    mkdir ${LOGS_ACCESS}/${DATE_DIR}
else
    mv ${LOGS_ACCESS}/access.log ${LOGS_ACCESS}/${DATE_DIR}/access_$DATE.log
fi

if [ ! -d ${LOGS_ERROR}/${DATE_DIR} ];then
    mkdir ${LOGS_ERROR}/${DATE_DIR}
else
    mv ${LOGS_ERROR}/error.log ${LOGS_ERROR}/${DATE_DIR}/error_$DATE.log
fi

#reload nginx
kill -USR1 `cat ${PID_PATH}`
